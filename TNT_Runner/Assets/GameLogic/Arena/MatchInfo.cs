﻿using UnityEngine;

[CreateAssetMenu(menuName = "Match/Match Info")]
public class MatchInfo : ScriptableObject
{
    public string roomName;
    public int roomCapacity;
}
