﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.Networking;

public class Arena : NetworkBehaviour {

    private bool isInit;

    [SerializeField]
    private GameObject spawnFloor;

    [SerializeField]
    private Animator countDown_animator;

    private static Arena _singleton;

    private List<MatchEntity> players;

    private static Arena Singleton
    {
        get
        {
            if (_singleton == null)
                _singleton = FindObjectOfType<Arena>();

            return _singleton;
        }
    }

    public static void ArenaSpawnPlayers(List<MatchEntity> players)
    {
        Singleton.players = players;

        Singleton.StartCoroutine(Singleton.CountDown());
    }

    private IEnumerator CountDown()
    {
        yield return new WaitForSeconds(3);

        CmdSpawnPlayers();
    }

    private IEnumerator UnfreezingPlayers()
    {
        countDown_animator.SetTrigger("CountDown");
        yield return new WaitForSeconds(3);

        UnfreezerPlayer();
    }

    [Command]
    private void CmdSpawnPlayers()
    {
        if (!Singleton.isInit)
        {
            Singleton.isInit = true;

            RpcTeleportPlayers();
        }
    }

    [ClientRpc]
    private void RpcTeleportPlayers()
    {
        Vector3 spawnPos = Singleton.spawnFloor.transform.position;
        spawnPos.x += Random.Range(-10, 10);
        spawnPos.z += Random.Range(-10, 10);
        spawnPos.y += 7;

     //   GameManager.Singleton.myPlayer.transform.position = spawnPos;

     //   GameManager.Singleton.myPlayer.FreezePlayer();

        StartCoroutine(UnfreezingPlayers());
    }

    private void UnfreezerPlayer()
    {
      //  GameManager.Singleton.myPlayer.UnfreezePlayer();
    }
}
