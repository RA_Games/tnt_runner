﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections.Generic;

public class MatchHandler : NetworkBehaviour
{
    [SerializeField]
    MatchInfo matchInfo;

    public List<MatchEntity> playersInMatch = new List<MatchEntity>();

    public void UpdatePlayersList()
    {
        playersInMatch.Clear();
        playersInMatch.AddRange(FindObjectsOfType<MatchEntity>());
    }

}
