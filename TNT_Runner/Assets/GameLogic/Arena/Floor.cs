﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class Floor : NetworkBehaviour
{
    [SyncVar]
    public bool isActive;

    [SyncVar(hook = "OnChangeHealth")]
    public uint health = 5;

    public void Pressed()
    {
        if(!isActive)
        {
            isActive = true;
            StartCoroutine(Destroing());
        }
    }

    private IEnumerator Destroing()
    {
        while (health > 0)
        {
            health--;
            yield return new WaitForSeconds(1);
        }
    }

    private void OnChangeHealth(uint value)
    {
        if (value == 0)
        {
            gameObject.SetActive(false);
        }
    }
   
}
