﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCamera : MonoBehaviour {

    [SerializeField]
    private Transform playerTransform;
    public float forward = 3;
    public float upward = 3;
    public float speed = 5;

    void LateUpdate()
    {
        if (playerTransform != null)
        {
            transform.position = Vector3.Lerp(transform.position, playerTransform.position - (playerTransform.forward * forward) + (playerTransform.up * upward), Time.deltaTime * speed);

            transform.LookAt(playerTransform.position + (playerTransform.up * 1.5f), playerTransform.up);
        }
    }

    public void SetTarget(Transform target)
    {
        playerTransform = target;
    }
}
