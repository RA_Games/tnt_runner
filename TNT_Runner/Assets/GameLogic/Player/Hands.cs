﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hands : MonoBehaviour {

    [SerializeField]
    BoolValue _isGrounded;

    void OnCollisionEnter(Collision collision)
    {
        print("collistion");
        if (!_isGrounded.value)
        {
            foreach (ContactPoint contact in collision.contacts)
            {
                if (contact.thisCollider.GetComponent<GrabPoint>() != null)
                {
                    print("Grab");
                }
            }
        }
    }
}
