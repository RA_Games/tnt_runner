﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class Motor : MonoBehaviour
{
    [SerializeField]
    InputCollection inputManager;

    public float speed = 5;

    private Vector3 _velocity;

    private Rigidbody _rb;

    private bool _isLocked = false;

    float hAxis;
    float vAxis;

    void Start()
    {
        _rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        if (!_isLocked)
        {
            hAxis = inputManager.horizontalAxis.value;
            vAxis = inputManager.verticalAxis.value;
        }
        else
        {
            hAxis = 0;
            vAxis = 0;
        }
    }

    void FixedUpdate()
    {
        Vector3 moveHorizontal = transform.right * hAxis;
        Vector3 moveVertical = transform.forward * vAxis;

        _velocity = (moveHorizontal + moveVertical).normalized * speed;

        _rb.MoveRotation(Camera.main.transform.rotation);

        if (_velocity != Vector3.zero)
        {
            _rb.MovePosition(_rb.position + _velocity * Time.fixedDeltaTime);
        }
    }

    protected void LateUpdate()
    {
        transform.localEulerAngles = new Vector3(0, transform.localEulerAngles.y, 0);
    }

    public void LockMotor()
    {
        _isLocked = true;
    }

    public void UnlockMotor()
    {
        _isLocked = false;
    }
}
