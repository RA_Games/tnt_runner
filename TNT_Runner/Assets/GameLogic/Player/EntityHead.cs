﻿using UnityEngine;
using System.Collections;

public class EntityHead : MonoBehaviour
{
    [SerializeField]
    InputCollection inputCollection;

    public float sensitivity = 3;

    [SerializeField]
    float yLimit = 70;

    [SerializeField]
    float xLimit = 70;

    private float x = 0.0f;
    private float y = 0.0f;

    private float clampminx;
    private float clampmaxx;

    void Start()
    {
        var angles = transform.eulerAngles;
        x = angles.y;
        y = angles.x;
    }

    void Update()
    {
        x += inputCollection.mouseX.value * sensitivity;
        y -= inputCollection.mouseY.value * sensitivity;

        clampminx = transform.eulerAngles.y - xLimit; 
        clampmaxx = transform.eulerAngles.y + xLimit;

        var tempx = ClampAngle(x, clampminx, clampmaxx); 

        if (x > tempx + 100) x -= 360; 
        else if (x < tempx - 100) x += 360; 

        x = ClampAngle(x, clampminx, clampmaxx); 

        y = ClampAngle(y, -yLimit, yLimit);

        var rotation = Quaternion.Euler(y, x, 0);

        transform.rotation = rotation;
    }

    float ClampAngle(float angle, float min, float max)
    {
        return Mathf.Clamp(angle, min, max);
    }
}
