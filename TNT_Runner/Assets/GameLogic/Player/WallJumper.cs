﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody), typeof(Collider))]
public class WallJumper : MonoBehaviour {

    [SerializeField]
    InputCollection inputCollection;

    [SerializeField]
    GameEvent eventWallAtacched;

    [SerializeField]
    GameEvent eventWallDetach;

    [SerializeField]
    private BoolValue _isGrounded;

    [Range(0, 3)]
    public float grabCooldown = 1;

    [Range(0, 1)]
    public float wallJumpFlow = 0.3f;

    private Rigidbody _rb;
    private Collider _coll;
    private bool _isLocked;
    private bool _isGrabbed;

    private float _grabCooldownStart = 0;
    private float _wallJumpFlowStart = 0;

    private ContactPoint wallContactPoint;
    private Vector3 lastFrameVelocity;
    private Vector3 lastFramePosition;
    private Vector3 grabbDirection;
    private Vector3 wallGrabPoint;

    void Start()
    {
        _rb = GetComponent<Rigidbody>();
        _coll = GetComponent<Collider>();
    }

    void Update ()
    {
        if (_isLocked)
            return;

        if (!_isGrounded.value)
        {
            if (_isGrabbed)
            {
                GrabWall();
            }
        }

        if(!_isGrabbed)
        {
            lastFrameVelocity = (transform.position - lastFramePosition) / Time.deltaTime;
            lastFramePosition = transform.position;
        }
    }

    private void GrabWall()
    {
        transform.position = wallGrabPoint;

        if (Time.time < _wallJumpFlowStart + wallJumpFlow)
        {
            if (inputCollection.isJumping.value)
            {
                JumpFlow();
            }
        }
        else
        {
            if (inputCollection.isJumping.value)
            {
                Jump();
            }
        }
    }

    private void JumpFlow()
    {
        Vector3 reflectAngle = Vector3.Reflect(grabbDirection, wallContactPoint.normal);

        Ray ray = new Ray(wallContactPoint.point, reflectAngle);

        float force = Mathf.Clamp(lastFrameVelocity.magnitude, 0f, 3f);

        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, force * 10))
        {
            wallGrabPoint = hit.point;
        }

        DetachWall();

        _rb.AddForce((reflectAngle + Vector3.up) * force, ForceMode.VelocityChange);
    }

    private void Jump()
    {
        DetachWall();

        _rb.velocity = Vector3.zero;

        Vector3 jumpDirection = new Vector3(inputCollection.horizontalAxis.value, 1 , inputCollection.verticalAxis.value).normalized;

        _rb.AddRelativeForce(jumpDirection * 10, ForceMode.VelocityChange);
    }

    public void DetachWall()
    {
        _isGrabbed = false;
        _rb.isKinematic = false;
        _grabCooldownStart = Time.time;
    }

    private void AttachToWall(ContactPoint attachPoint)
    {
        _isGrabbed = true;
        _rb.isKinematic = true;
        wallContactPoint = attachPoint;
        grabbDirection = lastFrameVelocity.normalized;
        wallGrabPoint = transform.position;
        _wallJumpFlowStart = Time.time;

        eventWallAtacched.Raise();
    }

    void OnCollisionEnter(Collision collision)
    {
        if (!_isGrounded.value)
        {
            if (!_isGrabbed)
            {
                if (Time.time > _grabCooldownStart + grabCooldown)
                {
                    if (collision.contacts[0].normal != Vector3.up)
                    {
                        ContactPoint attachPoint = collision.contacts[0];
                        AttachToWall(attachPoint);
                    }
                }
            }
        }
    }

    public void Lock()
    {
        _isLocked = true;
    }

    public void Unlock()
    {
        _isLocked = false;
    }

    private void OnDrawGizmos()
    {
        if (_isGrabbed)
        {
            Gizmos.color = Color.green;

            Vector3 normal = wallContactPoint.point + (wallContactPoint.normal);

            Gizmos.DrawLine(wallContactPoint.point, normal);

            Gizmos.color = Color.red;

            Vector3 reflectAngle = Vector3.Reflect(grabbDirection, wallContactPoint.normal);

            Gizmos.DrawLine(wallContactPoint.point, wallContactPoint.point + reflectAngle);
            Gizmos.DrawLine(wallContactPoint.point, wallGrabPoint);
            Gizmos.DrawSphere(wallContactPoint.point, 0.2f);
        }
    }
}
