﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections.Generic;

public class MatchEntity : NetworkBehaviour
{
    [SerializeField]
    private GameEvent eventStartPlayer;

    [SerializeField]
    private GameEvent eventPausePressed;

    [SerializeField]
    private GameEvent eventLockMotor;

    [SerializeField]
    private GameEvent eventUnlockMotor;

    private bool isAlive = true;

    public bool IsAlive
    {
        get
        {
            return isAlive;
        }
    }

    public void KillPlayer()
    {
        isAlive = false;
        Disconnect();
    }

    private void Disconnect()
    {
        NetworkManager.singleton.client.Disconnect();
    }

    public override void OnStartClient()
    {
        base.OnStartClient();
    }

    public override void OnStartLocalPlayer()
    {
        Camera.main.GetComponent<PlayerCamera>().SetTarget(gameObject.transform.GetComponentInChildren<EntityHead>().transform);
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            eventPausePressed.Raise();
        }
    }

    void Start()
    {
        CmdRegisterEntity(this.gameObject);
    }

    public void FreezePlayer()
    {
        eventLockMotor.Raise();
    }

    public void UnfreezePlayer()
    {
        eventUnlockMotor.Raise();
    }

    [Command]
    private void CmdRegisterEntity(GameObject entity)
    {
        eventStartPlayer.Raise();
    }

    void OnCollisionEnter(Collision coll)
    {
        if (coll.gameObject.tag.Equals("Floor"))
        {
            try
            {
                coll.gameObject.GetComponent<Floor>().Pressed();
            }
            catch
            {
            }
        }
    }

}
