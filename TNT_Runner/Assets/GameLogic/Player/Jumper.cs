﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody), typeof(Collider))]
public class Jumper : MonoBehaviour
{
    [SerializeField]
    InputCollection inputCollection;

    [SerializeField]
    GameEvent eventLockMotor;

    [SerializeField]
    GameEvent eventUnlockMotor;

    [SerializeField]
    GameEvent eventTouchGround;

    [SerializeField]
    GameEvent eventJump;

    [Range(0, 10)]
    public float jumpForce = 3.5f;

    public bool preventMovementOnJump;

    private Rigidbody _rb;
    private Collider _coll;

    [SerializeField]
    private BoolValue _isGrounded;

    private bool _isLocked = false;

    void Start()
    {
        _rb = GetComponent<Rigidbody>();
        _coll = GetComponent<Collider>();
        _isGrounded.value = true;
    }

    void Update()
    {
        DetectGround();
    }

    private void DetectGround()
    {
        Vector3 foot = _coll.bounds.center;
        float halfSize = _coll.bounds.size.y / 2;
        foot.y -= halfSize;

        Ray ray = new Ray(_coll.bounds.center, -_coll.transform.up);

        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, (_coll.bounds.size.y / 2) + 0.1f))
        {
            if (hit.normal == Vector3.up)
            {
                if (!_isGrounded.value)
                {
                    _isGrounded.value = true;
                    eventUnlockMotor.Raise();
                    eventTouchGround.Raise();
                }

                return;
            }
        }

        _isGrounded.value = false;
    }

    void FixedUpdate()
    {
        if (_isLocked)
        return;

        if (_isGrounded.value)
        {
            if (inputCollection.isJumping.value)
            {
                Jump();
            }
        }
    }

    private void Jump()
    {
        eventJump.Raise();

        _rb.AddForce((Vector3.up * jumpForce), ForceMode.Impulse);
        _isGrounded.value = false;

        if (preventMovementOnJump)
        {
            eventLockMotor.Raise();
        }
    }

    public void LockJump()
    {
        _isLocked = true;
    }

    public void UnlockJump()
    {
        _isLocked = false;
    }
}
