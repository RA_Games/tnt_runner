﻿using UnityEngine;

[CreateAssetMenu(menuName = "Values/Bool")]
public class BoolValue : ScriptableObject
{
    public bool value;
}
