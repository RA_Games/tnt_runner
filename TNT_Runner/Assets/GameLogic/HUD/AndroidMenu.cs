﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AndroidMenu : MonoBehaviour {

    public Joystick leftJoystick;
    public Joystick rightJoystick;

    [SerializeField]
    private GameEvent eventPausePressed;

    public void OpenConfig()
    {
        eventPausePressed.Raise();
    }
}
