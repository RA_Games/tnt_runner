﻿
using UnityEngine;
using UnityEngine.Networking;

public class PauseMenu : NetworkBehaviour
{
    [SerializeField]
    private GameEvent eventClosePause;
    [SerializeField]
    private GameObject clientUI;
    [SerializeField]
    private GameObject serverUI;

    public void Resume()
    {
        eventClosePause.Raise();
    }

    private void ShowCursor()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }

    private void Hideursor()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    public void Disconnect()
    {
    //    GameManager.Singleton.DisconnectFromMatch();
        Resume();
    }

    public void OpenPauseMenu()
    {
        if (isServer)
        {
            serverUI.SetActive(true);
        }
        else
        {
            clientUI.SetActive(true);
        }

        ShowCursor();
    }

    public void ClosePauseMenu()
    {
        serverUI.SetActive(false);

        clientUI.SetActive(false);

        Hideursor();
    }

    public void ForceStart()
    {
      // GameManager.Singleton.RoomReady();
        Resume();
    }
}
