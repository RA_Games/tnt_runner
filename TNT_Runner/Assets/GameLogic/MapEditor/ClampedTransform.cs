﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class ClampedTransform : MonoBehaviour {

    Vector3 _lastPos;

    Vector3 ClampPos
    {
        get
        {
            Vector3 aux = _lastPos;

            aux.x = aux.x - (aux.x % 1);
            aux.y = aux.y - (aux.y % 0.5f);
            aux.z = aux.z - (aux.z % 1);

            return aux;

        }
    }
	
    private void OnEnable()
    {
        //Destroy(this);
    }

	void Update ()
    {
        if (transform.position != _lastPos)
        {
            _lastPos = transform.position;

            transform.position = ClampPos;
        }
	}
}
