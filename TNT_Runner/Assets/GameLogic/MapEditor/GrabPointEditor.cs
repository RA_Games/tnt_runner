﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class GrabPointEditor : MonoBehaviour {

    [MenuItem("MapEditor/Add Grab Points")]
    private static void AddGrabPoints()
    {
        GameObject grabPoints = new GameObject("Grab Points", typeof(GrabPoint));
        GameObject selectedObject = (GameObject) Selection.activeObject;
        Mesh mesh = selectedObject.GetComponent<MeshFilter>().sharedMesh;

        grabPoints.transform.SetParent(selectedObject.transform);

        grabPoints.transform.localPosition = Vector3.zero;

        BoxCollider horizontalGrabPoints = grabPoints.AddComponent<BoxCollider>();

        horizontalGrabPoints.size = new Vector3(mesh.bounds.size.x + (mesh.bounds.size.x * 0.1f), mesh.bounds.size.y - (mesh.bounds.size.y * 0.9f), mesh.bounds.size.z - (mesh.bounds.size.z * 0.1f));
        horizontalGrabPoints.center = new Vector3(mesh.bounds.center.x, mesh.bounds.center.y + ((mesh.bounds.size.y / 2) * 0.8f), mesh.bounds.center.z);

        BoxCollider verticalGrabPoints = grabPoints.AddComponent<BoxCollider>();

        verticalGrabPoints.size = new Vector3(mesh.bounds.size.x - (mesh.bounds.size.x * 0.1f), mesh.bounds.size.y - (mesh.bounds.size.y * 0.9f), mesh.bounds.size.z + (mesh.bounds.size.z * 0.1f));
        verticalGrabPoints.center = new Vector3(mesh.bounds.center.x, mesh.bounds.center.y + ((mesh.bounds.size.y / 2) * 0.8f), mesh.bounds.center.z);
    }

    [MenuItem("MapEditor/Add Grab Points", true)]
    private static bool AddGrabPointsValidation()
    {
        if(Selection.activeObject != null)
        {
            if (Selection.activeObject.GetType() == typeof(GameObject))
            {
                GameObject selectedObject = (GameObject)Selection.activeObject;
                MeshFilter mesh = selectedObject.GetComponent<MeshFilter>();

                if (mesh != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
            

        return false;
    }
}
