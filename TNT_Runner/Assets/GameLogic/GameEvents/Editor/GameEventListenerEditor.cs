﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(GameEvent))]
[CanEditMultipleObjects]
public class GameEventListenerEditor: Editor  {

    private GameEvent myTarget;

    private void OnEnable()
    {
        myTarget = (GameEvent)target;
    }

    public override void OnInspectorGUI()
    {
        if (GUILayout.Button(myTarget.name))
        {
            myTarget.Raise();
        }
    }

}
