﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;

public class JoinGame : MonoBehaviour {

    private NetworkManager networkManager;

    private List<GameObject> roomList = new List<GameObject>();

    [SerializeField]
    private Text status_text;

    [SerializeField]
    private GameObject matchSlot_prefab;

    [SerializeField]
    private Transform content;

    void Start ()
    {
        networkManager = NetworkManager.singleton;

        if (networkManager.matchMaker == null)
        {
            networkManager.StartMatchMaker();
        }

        RefreshRoomList();

        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }
	
	public void RefreshRoomList ()
    {
        ClearRoomList();

        networkManager.matchMaker.ListMatches(0, 20, "", false, 0, 0, OnMatchList);
        status_text.text = "Loading...";
    }

    void OnMatchList(bool success, string extendedInfo, List<MatchInfoSnapshot> matches)
    {
        status_text.text = string.Empty;

        if (matches == null)
        {
            status_text.text = "Could't find any match";

            return;
        }

        foreach (MatchInfoSnapshot match in matches)
        {
            GameObject slot = Instantiate(matchSlot_prefab, content);

            MatchSlot item = slot.GetComponent<MatchSlot>();

            item.Setup(match, JoinRoom);

            roomList.Add(slot);
        }

        if(roomList.Count == 0)
        {
            status_text.text = "No Matches at the moment";
        }
    }

    public void JoinRoom(MatchInfoSnapshot _match)
    {
        networkManager.matchMaker.JoinMatch(_match.networkId, "", "", "", 0, 0, networkManager.OnMatchJoined);
        ClearRoomList();

    //    GameManager.Singleton.currentRoomName = _match.name;
    //    GameManager.Singleton.currentRoomSize = _match.maxSize;
        status_text.text = "JOINING";
    }

    private void ClearRoomList()
    {
        for (int i = 0; i < roomList.Count; i++)
        {
            Destroy(roomList[i]);
        }

        roomList.Clear();
    }
}
