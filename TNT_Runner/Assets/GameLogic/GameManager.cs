﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking.Match;

public class GameManager : MonoBehaviour
{
    public string currentRoomName;
    public int currentRoomSize;

    public Dictionary<string, MatchEntity> players = new Dictionary<string, MatchEntity>();

    public void RegisterPlayer(string netID, MatchEntity player)
    {
        players.Add(netID, player);

        if (players.Count == currentRoomSize)
        {
            RoomReady();
        }
    }

    public void UnregisterPlayer(string netID)
    {
        players.Remove(netID);
    }

    public void Start()
    {
        DontDestroyOnLoad(this);
    }

    public void RoomReady()
    {
        NetworkManager.singleton.matchMaker.SetMatchAttributes(NetworkManager.singleton.matchInfo.networkId, false, 0, NetworkManager.singleton.OnSetMatchAttributes);
    }

    public void OnRoomReady()
    {
        List<MatchEntity> players = new List<MatchEntity>(this.players.Values);
        Arena.ArenaSpawnPlayers(players);
    }

    public void DisconnectFromMatch()
    {
        ////if (myPlayer.isServer)
        ////{
        ////    NetworkManager.singleton.StopHost();
        ////    ClientScene.ClearSpawners();
        ////    ClientScene.DestroyAllClientObjects();
        ////    NetworkManager.singleton.StopMatchMaker();

        ////    Network.Disconnect();
        ////    MasterServer.UnregisterHost();

        ////    NetworkManager.singleton.StartMatchMaker();
        ////}
        ////else
        ////{
        ////    myPlayer.GetComponent<NetworkIdentity>().connectionToClient.Disconnect();
        ////}
    }

}
