﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(GameSystem))]
public class GameSystemsEditor : Editor {

    GameSystem myTarget;

    private void OnEnable()
    {
        myTarget = (GameSystem)target;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if (GUILayout.Button("Build in scene"))
        {
            myTarget.Build("[" + myTarget.name + "] ");
        }
    }
}

[CustomEditor(typeof(CoreGameSystem))]
public class CoreGameSystemsEditor : Editor
{
    CoreGameSystem myTarget;

    private void OnEnable()
    {
        myTarget = (CoreGameSystem)target;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if (GUILayout.Button("Build in scene"))
        {
            myTarget.Build("[" + myTarget.name);
        }
    }
}
