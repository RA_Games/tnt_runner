﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Systems/Game System")]
public class GameSystem : ScriptableObject {

    public List<GameObject> dependencies = new List<GameObject>();

    public void Build(string prefix)
    {
        foreach (GameObject dependency in dependencies)
        {
            GameObject instance = Instantiate(dependency);

            instance.name = prefix + " -> " + dependency.name;
        }
    }
}
