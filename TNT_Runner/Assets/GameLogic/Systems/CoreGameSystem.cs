﻿using UnityEngine;
using System.Collections.Generic;

[CreateAssetMenu(menuName = "Systems/Core System")]
public class CoreGameSystem : ScriptableObject
{
    public List<GameSystem> requirements = new List<GameSystem>();

    public void Build(string prefix)
    {
        foreach (GameSystem requirement in requirements)
        {
            requirement.Build(prefix + " [" + requirement.name + "] ]");
        }
    }
}
