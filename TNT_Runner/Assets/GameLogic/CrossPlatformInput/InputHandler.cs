﻿using UnityEngine;

public class InputHandler : MonoBehaviour
{
    [SerializeField]
    InputCollection inputManager;

    void Update()
    {
        inputManager.horizontalAxis.value = Input.GetAxis("Horizontal");

        inputManager.verticalAxis.value = Input.GetAxis("Vertical");

        inputManager.mouseX.value = Input.GetAxisRaw("Mouse X");

        inputManager.mouseY.value = Input.GetAxisRaw("Mouse Y");

        inputManager.isJumping.value = Input.GetButtonDown("Jump");
    }

}
