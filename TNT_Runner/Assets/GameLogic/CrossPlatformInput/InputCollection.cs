﻿using UnityEngine;

[CreateAssetMenu(menuName = "Input Manager/Input Collection")]
public class InputCollection : ScriptableObject
{
    public FloatValue horizontalAxis;
    public FloatValue verticalAxis;
    public FloatValue mouseX;
    public FloatValue mouseY;
    public BoolValue isJumping;


}
