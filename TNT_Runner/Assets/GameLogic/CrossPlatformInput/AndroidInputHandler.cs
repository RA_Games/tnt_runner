﻿using UnityEngine;

public class AndroidInputHandler : MonoBehaviour
{
    [SerializeField]
    InputCollection inputManager;

    [SerializeField]
    Joystick leftJoystick;

    [SerializeField]
    Joystick rightJoystick;

    float accelerometerUpdateInterval = 1.0f / 60.0f;
    float lowPassKernelWidthInSeconds = 1.0f;
    float shakeDetectionThreshold = 2.0f;
    float lowPassFilterFactor;
    Vector3 lowPassValue;

    void Start()
    {
        lowPassFilterFactor = accelerometerUpdateInterval / lowPassKernelWidthInSeconds;
        shakeDetectionThreshold *= shakeDetectionThreshold;
        lowPassValue = Input.acceleration;
    }

    void Update()
    {
        inputManager.horizontalAxis.value = leftJoystick.Horizontal;

        inputManager.verticalAxis.value = leftJoystick.Vertical;

        inputManager.mouseX.value = rightJoystick.Horizontal;

        inputManager.mouseY.value = rightJoystick.Vertical;

        Acelerometer();
    }

    private void Acelerometer()
    {
        inputManager.isJumping.value = false;

        Vector3 acceleration = Input.acceleration;
        lowPassValue = Vector3.Lerp(lowPassValue, acceleration, lowPassFilterFactor);
        Vector3 deltaAcceleration = acceleration - lowPassValue;

        if (deltaAcceleration.sqrMagnitude >= shakeDetectionThreshold)
        {
            inputManager.isJumping.value = true;
        }
    }
}
