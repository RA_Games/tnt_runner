﻿using UnityEngine;

public class InputHandlerManager : MonoBehaviour
{
    [SerializeField]
    GameObject inputHandler;

    [SerializeField]
    GameObject androidInputHandler;

    private void OnEnable()
    {
#if UNITY_STANDALONE_WIN
        Instantiate(inputHandler);
#endif

#if UNITY_ANDROID
        Instantiate(androidInputHandler);
#endif
    }
}
